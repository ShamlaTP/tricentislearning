ExplicitName
------------
Is a configuration Parameter which can be added to a Module which allows a change in the TestStepValue name which reflects a technical Identification.

Index
------
Numbers objects that have the same properties, or do not have a unique identifier in the order they appear on the page, using #1,#2, #3 and so on, ExplictName allows Tricentis Tosca to steer these objects using the index, taking into account any dynamic chnages.

ResultCount
------------
IS a property that is the result of a counting process which can be used for all controls. RowCount and ColumnCount can also be used for tables.

Tbox Set Buffer
---------------
Is a standard Module which allows you to set a value to a Buffer Manually.

Repitition
---------
IS a parameter which instructs Tosca to repeat the steps within the specified level a specified number of times. We can specify the repitition on the TestStep folder level only, which tells Tricentis Tosca to repeat the steps within a folder.


How to use ResultCount
----------------------
1. Navigate to the TestStepValue
2. Set applicable Action Mode
3. Click the blue drop down arrow in the value cell
4. Enter ResultCount in the left textbox and select an operator and value

How to use ExplicitName
------------------------
1. Add Configuration Parameter "ExplicitName" to the Module Attribute
2. Set the value to True
3. Rename corresponding TestStepValue if applicable



